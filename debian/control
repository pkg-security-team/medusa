Source: medusa
Section: admin
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Adrian Alves <aalves@gmail.com>
Build-Depends: debhelper-compat (= 13),
 libssl-dev,
 libsvn-dev,
 libpq-dev,
 libssh2-1-dev,
 libgcrypt-dev,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://foofus.net/?page_id=51
Vcs-Git: https://salsa.debian.org/pkg-security-team/medusa.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/medusa

Package: medusa
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: fast, parallel, modular, login brute-forcer for network services
 Medusa is intended to be a speedy, massively parallel, modular, login
 brute-forcer. The goal is to support as many services which allow remote
 authentication as possible. The author considers following items as some of
 the key features of this application:
      * Thread-based parallel testing. Brute-force testing can be
        performed against multiple hosts, users or passwords
        concurrently.
      * Flexible user input. Target information (host/user/password) can
        be specified in a variety of ways. For example, each item can be
        either a single entry or a file containing multiple entries.
        Additionally, a combination file format allows the user to
        refine their target listing.
      * Modular design. Each service module exists as an
        independent .mod file. This means that no modifications are
        necessary to the core application in order to extend the
        supported list of services for brute-forcing.
